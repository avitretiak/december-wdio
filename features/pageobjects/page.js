const Page = require('./page');

module.exports = class Page {

    // Global selectors for navbar and mobile navigation button //
    get btnMobileNav () { return $('a.btn_mobile') }
    get navBar() { return $('nav.navigation_bar') }

}
