const Page = require('./page');

class BlogPage extends Page {

    url = 'https://blog.decemberlabs.com';

    get formHeaderText() { return $('form > h3') }
    get nameInput () { return $('#name') }
    get emailInput () { return $('#email') }
    get companyInput() { return $('#company') }
    get messageInput() { return $('#message') }
    get btnGetInTouchSubmit() { return $('input.contact-form-submit') }
    get formSubmissionIllustration() { return $$}

    // Finds a span with the set text on the page, used to find the form submission response message //
    findFormResponse(text) {
        return $('//span[text()="' + text + '"]')
    }

    // Function takes a field name, if it is among the required, but empty fields (which are highlighted in red), returns true //
    checkRequiredField (fieldName) {
        let highlightedFields = $$('div.warning > .w-input')
        for (let field of highlightedFields) {
            if (field.getProperty('name') == fieldName.toLowerCase()) {
                return true;
                break;
            }
        }
    }
}

module.exports = new BlogPage();
