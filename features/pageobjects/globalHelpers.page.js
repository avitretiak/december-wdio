const Page = require('./page');


class GlobalHelpers extends Page {

    // finds first clickable anchor based on text, avoids failure from not opening mobile nav menu if anchor with same text is contained inside //
    clickAnchorText (text) {
        let selector = '//a[text()="' + text + '"]';
        let anchors = $$(selector);
        let firstClickable;
        for (let anchor of anchors) {
            if (anchor.isClickable()) {
                firstClickable = anchor;
                break;
            };
        }
        firstClickable.click();
    }

}


module.exports = new GlobalHelpers();