# December Labs WebdriverIO automation
This test suite includes both a positive and negative test case for the "Get in Touch" form  
This test uses Cucumber, WebdriverIO and ChromeDriver, reporting is done using the Spec reporter


Cucumber feature files can be found in `/features`, these detail in plain English the steps the tests run through  
These steps are mapped to code in `/features/step-definitions/steps.js` and the relevant Page Object files can be found in `/features/pageobjects`

Tests should be viewport size agnostic, succeeding in both mobile and desktop sized windows.


## Requirements
- npm v6.14.8
- node v12.18.2
- Google Chrome v87.0

## Usage

1. Clone the repository

`git clone https://gitlab.com/avitretiak/december-wdio.git`

2. Enter the directory

`cd december-wdio`

3. Install dependencies via npm

`npm install`

4. Execute tests

`npx wdio`

5. Observe automated testing in chrome and output in console :) for more detailed output, verbosity can be set in wdio.conf.js

## Findings

1. Inconsistent capitalization, buttons for form are capitalized as "Get in touch", where the form has the title "Get In Touch" (might be intentional, noticed when text selection didn't match)
2. After some test runs, lots of images on the blog stopped loading, seems firebase limit was reached, sorry! :(
