Feature: December Labs Blog - Get in touch form

  Background:
    Given I am on the home page
    And I click the "Blog" link
    And The blog page loads
    And I click the "Get in touch" button
    And The "Get In Touch" form opens

  @positive
  Scenario Outline: Positive Testing - As a user, I can complete the form with valid data and submit it properly

    Given I fill in the name with "<name>", the email with "<email>", the company with "<company>" and the message with "<message>"
    And I submit the "Get In Touch" form
    Then I should see a message saying "Success! You did it!"

    Examples:
      | name     | email                 | company | message       |
      | Test     | test@decemberlabs.com | Test    | Test          |

  @negative
  Scenario: Negative Testing - As a user, submitting the form without data should should red borders around the required fields and NOT submit the form

    Given I submit the "Get In Touch" form
    Then I should see a red border around the "Email" field
    And I should see a red border around the "Message" field
    And The "Get In Touch" form should not have submitted