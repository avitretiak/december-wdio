const { Given, When, Then } = require('cucumber');
const globalHelpers = require('../pageobjects/globalHelpers.page');
const blogPage = require('../pageobjects/blog.page');
const homePage = require('../pageobjects/home.page');

const pages = {
    home: homePage,
    blog: blogPage
}

Given(/^I am on the (\w+) page$/, (page) => {
    let targetPage = pages[page];
    browser.url(targetPage.url);
});

Given(/^The (\w+) page loads$/, (page) => {
    let targetPage = pages[page];
    targetPage.navBar.waitForDisplayed();
});

Given(/^I click the "([^\"]*)" (?:button|link)$/, (btnText) => {
    globalHelpers.clickAnchorText(btnText);
});

When(/^The "([^\"]*)" form (?:opens|should not have submitted)$/, (formTitle) => {
    blogPage.formHeaderText.waitForDisplayed();
    expect(blogPage.formHeaderText).toHaveTextContaining(formTitle);
});

When(/^I fill in the name with "([^\"]*)", the email with "([^\"]*)", the company with "([^\"]*)" and the message with "([^\"]*)"$/, (name, email, company, message) => {
    blogPage.nameInput.setValue(name);
    blogPage.emailInput.setValue(email);
    blogPage.companyInput.setValue(company);
    blogPage.messageInput.setValue(message);
});

When(/^I submit the "Get In Touch" form$/, () => {
    blogPage.btnGetInTouchSubmit.click();
});

Then(/^I should see a message saying "([^\"]*)"$/, (message) => {
    expect(blogPage.findFormResponse(message)).toBeVisibleInViewport();
});

Then(/^I should see a red border around the "([^\"]*)" field$/, (required) => {
    expect(blogPage.checkRequiredField(required)).toEqual(true);
});