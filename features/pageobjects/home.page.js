const Page = require('./page');

class HomePage extends Page {
    url = 'https://decemberlabs.com';
}

module.exports = new HomePage();
